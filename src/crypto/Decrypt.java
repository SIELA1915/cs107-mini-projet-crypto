package crypto;

import static crypto.Helper.bytesToString;
import static crypto.Helper.stringToBytes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

public class Decrypt {
	
	
	public static final int ALPHABETSIZE = Byte.MAX_VALUE - Byte.MIN_VALUE + 1 ; //256
	public static final int APOSITION = 97 + ALPHABETSIZE/2;
        public static final byte SPACE = 32;
	
	//source : https://en.wikipedia.org/wiki/Letter_frequency
	public static final double[] ENGLISHFREQUENCIES = {0.08497,0.01492,0.02202,0.04253,0.11162,0.02228,0.02015,0.06094,0.07546,0.00153,0.01292,0.04025,0.02406,0.06749,0.07507,0.01929,0.00095,0.07587,0.06327,0.09356,0.02758,0.00978,0.0256,0.0015,0.01994,0.00077};
	
	/**
	 * Method to break a string encoded with different types of cryptosystems
	 * @param type the integer representing the method to break : 0 = Caesar, 1 = Vigenere, 2 = XOR
	 * @return the decoded string or the original encoded message if type is not in the list above.
	 */
	public static String breakCipher(String cipher, int type) {
	        String plainText;
		byte[] cipherBytes = stringToBytes(cipher);

		switch (type) {
		case 0:
		    byte decodingKey = caesarWithFrequencies(cipherBytes);
		    plainText = bytesToString(Encrypt.caesar(cipherBytes, decodingKey));
		    break;
		case 1:
		    plainText = bytesToString(vigenereWithFrequencies(cipherBytes));
		    break;
		case 2:
		    plainText = arrayToString(xorBruteForce(cipherBytes));
		    break;
		default:
		    plainText = cipher;
		}
		
		return plainText;
	}
	
	
	/**
	 * Converts a 2D byte array to a String
	 * @param bruteForceResult a 2D byte array containing the result of a brute force method
	 */
	public static String arrayToString(byte[][] bruteForceResult) {
	        assert(bruteForceResult != null);

		String result = "";

		for (int i = 0; i < bruteForceResult.length; ++i) {
		    result += bytesToString(bruteForceResult[i]) + System.lineSeparator();
		}
		
		return result;
	}
	
	
	//-----------------------Caesar-------------------------
	
	/**
	 *  Method to decode a byte array  encoded using the Caesar scheme
	 * This is done by the brute force generation of all the possible options
	 * @param cipher the byte array representing the encoded text
	 * @return a 2D byte array containing all the possibilities
	 */
	public static byte[][] caesarBruteForce(byte[] cipher) {
	        assert(cipher != null);

		byte[][] plainTexts = new byte[ALPHABETSIZE][cipher.length];
		
		for (int i = 0, k = Byte.MIN_VALUE; i < ALPHABETSIZE; ++i, ++k) {
		    plainTexts[i] = Encrypt.caesar(cipher, (byte)(-k));
		}
		
		return plainTexts;
	}	
	
	
	/**
	 * Method that finds the key to decode a Caesar encoding by comparing frequencies
	 * @param cipherText the byte array representing the encoded text
	 * @return the decoding key
	 */
	public static byte caesarWithFrequencies(byte[] cipherText) {
	        assert(cipherText != null);

		float[] frequencies = computeFrequencies(cipherText);
		byte key = caesarFindKey(frequencies);
		
		return (byte)(-key);
	}
	
	/**
	 * Method that computes the frequencies of letters inside a byte array corresponding to a String
	 * @param cipherText the byte array 
	 * @return the character frequencies as an array of float
	 */
	public static float[] computeFrequencies(byte[] cipherText) {
	        assert(cipherText != null);

		float[] frequencies = new float[ALPHABETSIZE];
		int numCharsWithoutSpace = 0;

		for (int i = 0; i < cipherText.length; ++i) {
		    if (cipherText[i] == SPACE)
			continue;
		    ++numCharsWithoutSpace;
		    ++frequencies[cipherText[i] + ALPHABETSIZE/2];
		}

		for (float f : frequencies) {
		    f /= numCharsWithoutSpace;
		}

		//		System.out.println(Arrays.toString(frequencies));
		
		return frequencies;
	}
	
	
	/**
	 * Method that finds the key used by a  Caesar encoding from an array of character frequencies
	 * @param charFrequencies the array of character frequencies
	 * @return the key
	 */
	public static byte caesarFindKey(float[] charFrequencies) {
	        assert(charFrequencies != null);
		assert(charFrequencies.length == ALPHABETSIZE);

		double maxProd = 0;
		int maxIndex = -1;
		
		for (int i = 0; i < ALPHABETSIZE; ++i) {
		    double prod = 0;
		    for (int j = 0; j < ENGLISHFREQUENCIES.length; ++j) {
			prod += charFrequencies[(i+j)%ALPHABETSIZE] * ENGLISHFREQUENCIES[j];
		    }
		    if (prod > maxProd) {
			maxProd = prod;
			maxIndex = i;
		    }
		}
		
		return (byte)(maxIndex - APOSITION);
	}
	
	
	
	//-----------------------XOR-------------------------
	
	/**
	 * Method to decode a byte array encoded using a XOR 
	 * This is done by the brute force generation of all the possible options
	 * @param cipher the byte array representing the encoded text
	 * @return the array of possibilities for the clear text
	 */
	public static byte[][] xorBruteForce(byte[] cipher) {
		assert(cipher != null);

		byte[][] plainTexts = new byte[ALPHABETSIZE][cipher.length];
		
		for (int i = 0, k = Byte.MIN_VALUE; i < ALPHABETSIZE; ++i, ++k) {
		    plainTexts[i] = Encrypt.xor(cipher, (byte)k);
		}
		
		return plainTexts;
	}
	
	
	
	//-----------------------Vigenere-------------------------
	// Algorithm : see  https://www.youtube.com/watch?v=LaWp_Kq0cKs	
	/**
	 * Method to decode a byte array encoded following the Vigenere pattern, but in a clever way, 
	 * saving up on large amounts of computations
	 * @param cipher the byte array representing the encoded text
	 * @return the byte encoding of the clear text
	 */
	public static byte[] vigenereWithFrequencies(byte[] cipher) {
	        assert(cipher != null);
		assert(cipher.length != 0);

		List<Byte> cleanedCipher = removeSpaces(cipher);
		int keyLength = vigenereFindKeyLength(cleanedCipher);
		byte[] decodingKey = vigenereFindKey(cleanedCipher, keyLength);
		byte[] plainText = Encrypt.vigenere(cipher, decodingKey);
		
		return plainText;
	}
	
	
	
	/**
	 * Helper Method used to remove the space character in a byte array for the clever Vigenere decoding
	 * @param array the array to clean
	 * @return a List of bytes without spaces
	 */
	public static List<Byte> removeSpaces(byte[] array){
	        assert(array != null);

		List<Byte> cleaned = new ArrayList<Byte>();

		for (byte c : array) {
		    if (c != SPACE) {
			cleaned.add(c);
		    }
		}
		
		return cleaned;
	}

        /**
	 * Method that computes the number of coincidences for all the possible shifts between the text and itself.
	 * @param cipher the byte array representing the encoded text without space
	 * @return an array of the number of coincidences for each shift size
	 */
        public static int[] computeCoincidences(List<Byte> cipher) {
	    assert(cipher != null);

	    int[] coincidences = new int[cipher.size()-1];
	    
	    for (int i = 1; i < cipher.size(); ++i) {
		for (int j = 0; j < cipher.size()-i; ++j) {
		    if (cipher.get(j) == cipher.get(i+j)) {
			++coincidences[i-1];
		    }
		}
	    }

	    return coincidences;
	}

        /**
	 * Method that computes the local maxima based on the coincidences.
	 * @param coincidences the array of the number of coincidences for each shift size
	 * @return a List of the indices of the local maxima
	 */
        public static List<Integer> computeLocalMaxima(int[] coincidences) {
	    assert(coincidences != null);

	    List<Integer> localMaxima = new ArrayList<Integer>();

	    if (coincidences[0] > Math.max(coincidences[1], coincidences[2]))
		localMaxima.add(0);
	    //If coincidences[0] is a local maximum, coincidences[0] is bigger than coincidences[1]
	    //and coincidences[1] can't be a local maximum, so we can use else if instead of if
	    else if (coincidences[1] > Math.max(coincidences[2], Math.max(coincidences[3], coincidences[0])))
		localMaxima.add(1);

	    for (int i = 2; i < Math.ceil(coincidences.length/2); ++i) {
		int potMax = coincidences[i];
		if (potMax > Math.max(coincidences[i-2], Math.max(coincidences[i-1], Math.max(coincidences[i+1], coincidences[i+2])))) {
		    localMaxima.add(i);
		    //i += 2; //Since coincidences[i] is bigger than coincidences[i+2], coincidences[i+2] can't be a
		            //local maximum. So we add two, and with the ++i from the for loop only consider
		            //coincidences[i+3] as next potential local maximum
		}
	    }

	    return localMaxima;
	}

        /**
	 * Method that computes the most probable key length from the distances between local maxima
	 * @param localMaxima the list of the indices of the local maxima
	 * @return the most probable length of the key
	 */
        public static int computeProbableKeyLength(List<Integer> localMaxima) {
	    assert(localMaxima != null);

	    Map<Integer, Integer> occurencesOfDistance = new HashMap<>();

	    for (int i = 1; i < localMaxima.size(); ++i) {
		int dist = localMaxima.get(i)-localMaxima.get(i-1);
		occurencesOfDistance.merge(dist, 1, Integer::sum);
	    }

	    int maxDist = localMaxima.size();
	    int maxOccurences = -1;
	    
	    for (int dist : occurencesOfDistance.keySet()) {
		if (occurencesOfDistance.get(dist) > maxOccurences) {
		    maxDist = dist;
		    maxOccurences = occurencesOfDistance.get(dist);
		}
	    }

	    return maxDist;
	}
    
	/**
	 * Method that computes the key length for a Vigenere cipher text.
	 * @param cipher the byte array representing the encoded text without space
	 * @return the length of the key
	 */
	public static int vigenereFindKeyLength(List<Byte> cipher) {
	        assert(cipher != null);

		int[] coincidences = computeCoincidences(cipher);
		List<Integer> localMaxima = computeLocalMaxima(coincidences);

		int keyLength = computeProbableKeyLength(localMaxima);

		return keyLength;
	}

	
	
	/**
	 * Takes the cipher without space, and the key length, and uses the dot product with the English language frequencies 
	 * to compute the shifting for each letter of the key
	 * @param cipher the byte array representing the encoded text without space
	 * @param keyLength the length of the key we want to find
	 * @return the inverse key to decode the Vigenere cipher text
	 */
	public static byte[] vigenereFindKey(List<Byte> cipher, int keyLength) {
	        assert(cipher != null);

		byte[] key = new byte[keyLength];

		for (int i = 0; i < keyLength; ++i) {
		    int numChars = cipher.size()/keyLength;
		    if (cipher.size()%keyLength > i)
			++numChars;

		    byte[] cipherForKeyPos = new byte[numChars];
		    for (int j = 0; j < numChars; ++j) {
			cipherForKeyPos[j] = cipher.get(i + j*keyLength);
		    }

		    key[i] = caesarWithFrequencies(cipherForKeyPos);
		}
		
		return key;
	}
	
	
	//-----------------------Basic CBC-------------------------
	
	/**
	 * Method used to decode a String encoded following the CBC pattern
	 * @param cipher the byte array representing the encoded text
	 * @param iv the pad of size BLOCKSIZE we use to start the chain encoding
	 * @return the clear text
	 */
	public static byte[] decryptCBC(byte[] cipher, byte[] iv) {
	        assert(cipher != null);
		assert(iv != null);

		int BLOCKSIZE = iv.length;
		int numBlocks = (int)(cipher.length / BLOCKSIZE);

		byte[] key = iv;
		
		byte[] plainText = new byte[cipher.length];
		
		for (int i = 0; i < numBlocks; ++i) {
		    byte[] blockCipherText = new byte[BLOCKSIZE];
		    System.arraycopy(cipher, i*BLOCKSIZE, blockCipherText, 0, BLOCKSIZE);
		    byte[] blockPlainText = Encrypt.oneTimePad(blockCipherText, key);
		    System.arraycopy(blockPlainText, 0, plainText, i*BLOCKSIZE, BLOCKSIZE);
		    key = blockCipherText;
		}

		int numMissingChars = cipher.length % BLOCKSIZE;
		
		if (numMissingChars > 0) {
		    byte[] shortKey = new byte[numMissingChars];
		    System.arraycopy(key, 0, shortKey, 0, numMissingChars);
		    byte[] shortCipherText = new byte[numMissingChars];
		    System.arraycopy(cipher, numBlocks*BLOCKSIZE, shortCipherText, 0, numMissingChars);
		    byte[] shortPlainText = Encrypt.oneTimePad(shortCipherText, shortKey);
		    System.arraycopy(shortPlainText, 0, plainText, numBlocks*BLOCKSIZE, numMissingChars);
		}
		
		return plainText;
	}
	
	
	

		
		
		
		
		
}
