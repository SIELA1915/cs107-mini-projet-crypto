package crypto;

import static crypto.Helper.cleanString;
import static crypto.Helper.stringToBytes;
import static crypto.Helper.bytesToString;

/*
 * Part 1: Encode (with note that one can reuse the functions to decode)
 * Part 2: bruteForceDecode (caesar, xor) and CBCDecode
 * Part 3: frequency analysis and key-length search
 * Bonus: CBC with encryption, shell
 */
public class Main {
	
	
	//---------------------------MAIN---------------------------
	public static void main(String args[]) {
		

	        //Decrypt the challenge
	        /*	        String inputMessage = Helper.readStringFromFile("challenge-encrypted_new.txt");
			System.out.println(Decrypt.breakCipher(inputMessage, 1));*/

	        String inputMessage = Helper.readStringFromFile("text_three.txt");
		String key = "2cF%5";
		
		String messageClean = cleanString(inputMessage);

		byte[] messageBytes = stringToBytes(messageClean);
		byte[] keyBytes = stringToBytes(key);

		
		
		System.out.println("Original input sanitized : " + messageClean);
		System.out.println();
		
		System.out.println("------Caesar------");
		testCaesar(messageBytes, keyBytes[0]);
		

		System.out.println("-----Vigenere-----");
		testVigenere(messageBytes, keyBytes);

		System.out.println("-------XOR--------");
		testXor(messageBytes, keyBytes[0]);

		System.out.println("-------OTP--------");
		testOneTimePad(messageBytes);

		System.out.println("-------CBC--------");
		testCBC(messageBytes);
      	}
	
	
	//Run the Encoding and Decoding using the caesar pattern 
	public static void testCaesar(byte[] string , byte key) {
		//Encoding
		byte[] result = Encrypt.caesar(string, key);
		String s = bytesToString(result);
		String s2 = Encrypt.encrypt(bytesToString(string), bytesToString(new byte[]{key, 0}), 0);
		assert(s.equals(s2));
		System.out.println("Encoded : " + s);

		//Decoding with key
		String sD = bytesToString(Encrypt.caesar(result, (byte) (-key)));
		System.out.println("Decoded knowing the key : " + sD);

		//Decoding without key
		byte[][] bruteForceResult = Decrypt.caesarBruteForce(result);
		String sDA = Decrypt.arrayToString(bruteForceResult);
		Helper.writeStringToFile(sDA, "bruteForceCaesar.txt");

		byte decodingKey = Decrypt.caesarWithFrequencies(result);
		String sFD = bytesToString(Encrypt.caesar(result, decodingKey));
		String sFD2 = Decrypt.breakCipher(s, 0);
		assert(sFD.equals(sFD2));
		System.out.println("Decoded without knowing the key : " + sFD);
	}

        //Run the Encoding and Decoding using the vigenere pattern 
	public static void testVigenere(byte[] string , byte[] key) {
	        //Encoding
		byte[] result = Encrypt.vigenere(string, key);
		String s = bytesToString(result);
		String s2 = Encrypt.encrypt(bytesToString(string), bytesToString(key), 1);
		assert(s.equals(s2));
		System.out.println("Encoded : " + s);
		
		//Decoding with key
		byte[] inverseKey = key;
		for (int i = 0; i < key.length; ++i) {
		    inverseKey[i] = (byte)-key[i];
		}
		String sD = bytesToString(Encrypt.vigenere(result, inverseKey));
		System.out.println("Decoded knowing the key : " + sD);

		
		//Decoding without key
		String sFD = bytesToString(Decrypt.vigenereWithFrequencies(result));
		String sFD2 = Decrypt.breakCipher(s, 1);
		assert(sFD.equals(sFD2));
		System.out.println("Decoded without knowing the key : " + sFD);
	}

        //Run the Encoding and Decoding using the xor pattern 
        public static void testXor(byte[] string , byte key) {
		//Encoding
		byte[] result = Encrypt.xor(string, key);
		String s = bytesToString(result);
		String s2 = Encrypt.encrypt(bytesToString(string), bytesToString(new byte[]{key, 0}), 2);
		assert(s.equals(s2));
		System.out.println("Encoded : " + s);
		
		//Decoding with key
		String sD = bytesToString(Encrypt.xor(result, key));
		System.out.println("Decoded knowing the key : " + sD);
		
		//Decoding without key
		byte[][] xorForceResult = Decrypt.xorBruteForce(result);
		String sDA = Decrypt.arrayToString(xorForceResult);
		String sDA2 = Decrypt.breakCipher(s, 2);
		assert(sDA.equals(sDA2));
		Helper.writeStringToFile(sDA, "bruteForceXor.txt");
	}

        //Run the Encoding and Decoding using the one time pad
        public static void testOneTimePad(byte[] string) {
 	        byte[] pad = Encrypt.generatePad(string.length);

		//Encoding
		byte[] result = Encrypt.oneTimePad(string, pad);
		String s = bytesToString(result);
		String s2 = Encrypt.encrypt(bytesToString(string), bytesToString(pad), 3);
		assert(s.equals(s2));
		System.out.println("Encoded : " + s);

		//Decoding with key
		String sD = bytesToString(Encrypt.oneTimePad(result, pad));
		System.out.println("Decoded knowing the key : " + sD);
	}

        //Run the Encoding and Decoding using the CBC pattern
        public static void testCBC(byte[] string) {
	        byte[] iv = Encrypt.generatePad(string.length / 10);

		//Encoding
		byte[] result = Encrypt.cbc(string, iv);
		String s = bytesToString(result);
		String s2 = Encrypt.encrypt(bytesToString(string), bytesToString(iv), 4);
		assert(s.equals(s2));
		System.out.println("Encoded : " + s);

		//Decoding with key
		String sD = bytesToString(Decrypt.decryptCBC(result, iv));
		System.out.println("Decoded knowing the key : " + sD);
	}

//TODO : TO BE COMPLETED
	
}
